<?php
declare(strict_types=1);

if (!defined('TYPO3')) {
    die('Access denied.');
}

$array = [
    'types' => [
        'ical_feed' => [
            'showitem' => '
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general, type, title, activate_free_and_busy, free_and_busy_users_and_groups, ical_url, refresh_interval, scheduler_id,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden, starttime, endtime
            '
        ],
        'ical_file' => [
            'showitem' => '
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general, type, title, activate_free_and_busy, free_and_busy_users_and_groups, ical_file, refresh_interval, scheduler_id,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden, starttime, endtime
            '
        ]
    ],
    'columns' => [
        'ical_url' => [
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db.xlf:tx_calendar_calendar.ical_url',
            'config' => [
                'type' => 'user',
                'renderType' => 'calical-feedElement',
                'default' => ''
            ]
        ],
        'ical_file' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db.xlf:tx_calendar_calendar.ical_file',
            'config' => [
                'type' => 'group',
                'internal_type' => 'file',
                'allowed' => 'ics', // Must be empty for disallowed to work.
                'max_size' => 10000,
                'uploadfolder' => 'uploads/tx_cal/ics',
                'size' => 1,
                'fieldWizard' => [
                    'fileThumbnails' => [
                        'disabled' => true,
                    ]
                ],
                'autoSizeMax' => 1,
                'maxitems' => 1,
                'minitems' => 0,
                'default' => ''
            ]
        ],
        'type' => [
            'config' => [
                'items' => [
                    [
                        'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db.xlf:tx_calendar_calendar.type.I.feed',
                        'ical_feed'
                    ],
                    [
                        'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db.xlf:tx_calendar_calendar.type.I.file',
                        'ical_file'
                    ]
                ],
            ]
        ],
    ],
];
