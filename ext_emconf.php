<?php

$EM_CONF['calendar_icalendar'] = [
    'title' => 'Calendar Icalendar',
    'description' => 'A basic implementation of a working icalendar for ext:cal to uncouple the calendar from the view.',
    'category' => 'plugin',
    'version' => '1.0.0',
    'state' => 'stable',
    'author' => 'Jan Helke',
    'author_email' => 'calendar@typo3.helke.de',
    'constraints' => [
        'depends' => [
            'calendar_api' => '1.0 - 1.999'
        ],
    ]
];
