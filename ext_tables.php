<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

/**
 * Register icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

$iconRegistry->registerIcon(
    'tx-calendar-calendar-feed',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:calendar_icalendar/Resources/Public/Icons/tx_calendar_icalendar_ical-feed.svg']
);
$iconRegistry->registerIcon(
    'tx-calendar-calendar-file',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:calendar_icalendar/Resources/Public/Icons/tx_calendar_icalendar_ical-file.svg']
);
